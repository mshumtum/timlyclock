package com.amtech.timelyclock.Ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amtech.timelyclock.BuildConfig;
import com.amtech.timelyclock.R;
import com.amtech.timelyclock.Ui.dialog.SetRigtoneDialogFragment;
import com.amtech.timelyclock.Ui.genralScreen.AboutApp;
import com.amtech.timelyclock.Ui.genralScreen.ContactUsActivity;
import com.amtech.timelyclock.util.Constants;
import static com.amtech.timelyclock.util.Constants.FADE_BOOL;
import static com.amtech.timelyclock.util.Constants.FADE_IN_PREF;
import static com.amtech.timelyclock.util.Constants.RINGTONE_NAME;
import static com.amtech.timelyclock.util.Constants.RINGTONE_PREF;
import static com.amtech.timelyclock.util.Constants.RINGTONE_URI;
import static com.amtech.timelyclock.util.Constants.VIBRATION_BOOL;
import static com.amtech.timelyclock.util.Constants.VIBRATION_PREF;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener, SetRigtoneDialogFragment.OnDialogCompleteListener {
    private static final String TAG = "SettingActivity";
    RelativeLayout soundRL, soundClick,genralRL,aboutBtn,shareBtn,contactBtn;
    View sound_content,content_genral;
    ImageView btn_sound, back, setting,btn_genral;
    TextView ringtoneName,volumeLevel;
    boolean isTouched = false,isFade=false;
    SwitchCompat fade_switch,vibration_switch;
    SharedPreferences ringPref,vibrationPref,fadeInPref;
    SetRigtoneDialogFragment setRepeatDaysDialogFragment;
    SeekBar seekBar;
    Double percentage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ringPref= getApplicationContext().getSharedPreferences(RINGTONE_PREF, 0);
        vibrationPref= getApplicationContext().getSharedPreferences(VIBRATION_PREF, 0);
        fadeInPref= getApplicationContext().getSharedPreferences(FADE_IN_PREF, 0);
        setRepeatDaysDialogFragment = new SetRigtoneDialogFragment();

        initView();

        setVolume();

    }

    private void setVolume() {
        seekBar.getProgressDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        seekBar.getThumb().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);

        final AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int currentVolume = audio.getStreamVolume(AudioManager.STREAM_ALARM);
        final int max = audio.getStreamMaxVolume(AudioManager.STREAM_ALARM);
        Log.e(TAG, "setVolume: "+ currentVolume);
        Log.e(TAG, "setVolume: "+ max);
        seekBar.setMax(max);
        seekBar.setProgress(currentVolume);
        percentage = ((double)currentVolume/max) * 100;
        volumeLevel.setText(String.valueOf(percentage.intValue()));

        Log.e(TAG, "setVolume:percentage "+ percentage.intValue());
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Log.e(TAG, "setVolume: "+ i);
                percentage = ((double)i/max) * 100;
                volumeLevel.setText(String.valueOf(percentage.intValue()));
                audio.setStreamVolume(AudioManager.STREAM_ALARM, i, 0);
                int currentVolume = audio.getStreamVolume(AudioManager.STREAM_ALARM);
                Log.e(TAG, "onProgressChanged: "+currentVolume );
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    private void initView() {
        ScrollView mainBackground=findViewById(R.id.mainBackground);
        mainBackground.setBackgroundResource(Constants.getTheme());


        ringtoneName = findViewById(R.id.ringtoneName);
        genralRL = findViewById(R.id.genralRL);
        btn_genral = findViewById(R.id.btn_genral);
        contactBtn = findViewById(R.id.contactBtn);
        volumeLevel = findViewById(R.id.volumeLevel);
        soundRL = findViewById(R.id.soundRL);
        aboutBtn = findViewById(R.id.aboutBtn);
        shareBtn = findViewById(R.id.shareBtn);
        vibration_switch = findViewById(R.id.vibration_switch);
        seekBar = findViewById(R.id.seekBar);
        btn_sound = findViewById(R.id.btn_sound);
        setting = findViewById(R.id.setting);
        sound_content = findViewById(R.id.sound_content);
        content_genral = findViewById(R.id.content_genral);
        soundClick = findViewById(R.id.soundClick);
        fade_switch = findViewById(R.id.fade_switch);
        back = findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        setting.setVisibility(View.GONE);
        soundRL.setOnClickListener(this);
        soundClick.setOnClickListener(this);
        genralRL.setOnClickListener(this);
        aboutBtn.setOnClickListener(this);
        contactBtn.setOnClickListener(this);
        shareBtn.setOnClickListener(this);
     //   Toast.makeText(this, "Some function are unstable here in morning we will fix it!!", Toast.LENGTH_SHORT).show();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        fade_switch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                isFade = true;
                return false;
            }
        });
        fade_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isFade) {
                    isFade = false;
                    Log.e("TAG", "onCheckedChanged: "+isChecked );
                    if (isChecked) {
                        SharedPreferences.Editor editor = fadeInPref.edit();
                        editor.putBoolean(FADE_BOOL, true);
                        editor.commit();
                    } else {
                        SharedPreferences.Editor editor = fadeInPref.edit();
                        editor.putBoolean(FADE_BOOL, false);
                        editor.commit();
                    }
                }
            }
        });
     vibration_switch.setOnTouchListener(new View.OnTouchListener() {
         @Override
         public boolean onTouch(View view, MotionEvent motionEvent) {
             isTouched = true;
             return false;
         }
     });

        vibration_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isTouched) {
                    isTouched = false;
                    Log.e("TAG", "onCheckedChanged: "+isChecked );

                    if (isChecked) {
                        SharedPreferences.Editor editor = vibrationPref.edit();
                        editor.putBoolean(VIBRATION_BOOL, true);
                        editor.commit();
                    } else {
                        SharedPreferences.Editor editor = vibrationPref.edit();
                        editor.putBoolean(VIBRATION_BOOL, false);
                        editor.commit();
                    }
                }
            }
        });
        Log.e("TAG", "initView: "+vibrationPref.getBoolean(VIBRATION_BOOL,false) );
        Log.e("TAG", "initView: "+fadeInPref.getBoolean(FADE_BOOL,false) );

        ringtoneName.setText(ringPref.getString(RINGTONE_NAME, "Default"));
        vibration_switch.setChecked(vibrationPref.getBoolean(VIBRATION_BOOL,false));
        fade_switch.setChecked(fadeInPref.getBoolean(FADE_BOOL,false));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.soundRL:
                btn_sound.setImageResource(sound_content.isShown() ? R.drawable.ic_arrow_drop_down_black_24dp : R.drawable.ic_arrow_drop_up_black_24dp);
                if (sound_content.isShown()) {
                    sound_content.setVisibility(View.GONE);
                } else {
                    sound_content.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.soundClick:
                getSoundFromDialog();
                break;
            case R.id.aboutBtn:
                startActivity(new Intent(this, AboutApp.class));
                break;
            case R.id.contactBtn:
                startActivity(new Intent(this, ContactUsActivity.class));
                break;
            case R.id.shareBtn:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out my app at: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.genralRL:
                btn_genral.setImageResource(content_genral.isShown() ? R.drawable.ic_arrow_drop_down_black_24dp : R.drawable.ic_arrow_drop_up_black_24dp);
                if (content_genral.isShown()) {
                    content_genral.setVisibility(View.GONE);
                } else {
                    content_genral.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    void getSoundFromDialog() {
        setRepeatDaysDialogFragment.show(getSupportFragmentManager(), "A");
    }

    @Override
    public void onDialogComplete(String name, Uri uri) {
        setRepeatDaysDialogFragment.dismiss();
        ringtoneName.setText(name);
        SharedPreferences.Editor editor = ringPref.edit();
        editor.putString(RINGTONE_NAME, name);
        editor.putString(RINGTONE_URI, "" + uri);
        editor.commit();
    }
}
