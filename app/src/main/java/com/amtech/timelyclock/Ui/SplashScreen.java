package com.amtech.timelyclock.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.amtech.timelyclock.R;
import com.amtech.timelyclock.util.Constants;
import static com.amtech.timelyclock.util.Constants.darkBlue;
import static com.amtech.timelyclock.util.Constants.lightBlue;
public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        View layout = findViewById(R.id.splashBackground);



        layout.setBackgroundResource(Constants.getTheme());

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over
                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }, 3000);
    }
}
