package com.amtech.timelyclock.Ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.amtech.timelyclock.R;
import com.amtech.timelyclock.Ui.AddAlarmActivity;
import com.amtech.timelyclock.Ui.MainActivity;
import com.amtech.timelyclock.alarmmanager.AlarmHandler;
import com.amtech.timelyclock.db.Alarm;
import com.amtech.timelyclock.viewmodel.AlarmViewModel;

import java.util.Collections;
import java.util.List;

import static com.amtech.timelyclock.Ui.MainActivity.NEW_ALARM_ACTIVITY_REQUEST_CODE;

/**
 * ArrayAdapter used to populate MainActivity Alarms ListView
 */
public class AlarmListAdapter extends RecyclerView.Adapter<AlarmListAdapter.AlarmViewHolder> {

    private final String TAG = "AlarmListAdapter";
    boolean isFade=false;
    /**
     * View for each alarm
     */
    class AlarmViewHolder extends RecyclerView.ViewHolder {
        private final TextView timeTextView;
        private final TextView repetitionTextView;
        private final Switch activeSwitch;
        private final ImageButton editButton;




        private AlarmViewHolder(View itemView) {
            super(itemView);
            timeTextView = itemView.findViewById(R.id.alarm_time_text);
            repetitionTextView = itemView.findViewById(R.id.alarm_repetition_text);
            activeSwitch = itemView.findViewById(R.id.alarm_active_switch);
            editButton = itemView.findViewById(R.id.alarm_edit_button);
        }
    }

    // Layout members
    private final LayoutInflater mInflater;
    // Data list (cached copy of alarms)
    private List<Alarm> mAlarms = Collections.emptyList();
    // To handle interactions with database
    private AlarmViewModel mAlarmViewModel;
    // To schedule alarms
    private AlarmHandler mAlarmHandler;

    public AlarmListAdapter(final Context context) {
        mInflater = LayoutInflater.from(context);

        mAlarmViewModel = ViewModelProviders.of((MainActivity) context).get(AlarmViewModel.class);
        mAlarmHandler = new AlarmHandler(context, ((MainActivity) context).findViewById(R.id.mainBackground));

        setHasStableIds(true); // so Switch interaction has smooth animations



    }

    @NonNull
    @Override
    public AlarmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_alarm, parent, false);
        return new AlarmViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AlarmViewHolder viewHolder, int position) {
        Resources resources = viewHolder.itemView.getContext().getResources();
        Alarm alarm = mAlarms.get(position);

        viewHolder.timeTextView.setText(alarm.getStringTime()); // set alarm time

        // Set repeatTextView text
        if (alarm.isRepeating()) {
            String repetitionText = alarm.getStringOfActiveDays();
            viewHolder.repetitionTextView.setText(repetitionText);
        } else {
            viewHolder.repetitionTextView.setText(resources.getString(R.string.no_repeat));
        }

        // Set TextView colors based on alarm's active state
        if (alarm.isActive()) {
            viewHolder.activeSwitch.setChecked(true);
            viewHolder.timeTextView.setTextColor(resources.getColor(R.color.colorAccent));
            viewHolder.repetitionTextView.setTextColor(resources.getColor(R.color.colorDarkText));
        } else {
            viewHolder.activeSwitch.setChecked(false);
            viewHolder.timeTextView.setTextColor(resources.getColor(R.color.colorGrey500));
            viewHolder.repetitionTextView.setTextColor(resources.getColor(R.color.colorGrey500));
        }

        // Add Button click listeners
        addSwitchListener(alarm, viewHolder, resources);
        addEditButtonListener(alarm, viewHolder);
    }

    /**
     * Update current list of alarms and update UI
     *
     * @param alarms List of updated alarms
     */
    public void setAlarms(List<Alarm> alarms) {
        Log.i(TAG, "updating alarms data-set");
        this.mAlarms = alarms;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
       // tabHereLL.setVisibility(mAlarms.size() > 0 ? View.GONE : View.VISIBLE);
        return mAlarms.size();
    }

    @Override
    public long getItemId(int position) {
        return mAlarms.get(position).id;
    }

    /**
     * Add OnCheckedChange Listener to alarm's "active" switch
     *
     * @param alarm      Alarm object
     * @param viewHolder Alarm's ViewHolder object, containing Switch
     * @param resources  Resources file to get color values from
     */
    private void addSwitchListener(final Alarm alarm, final AlarmViewHolder viewHolder, final Resources resources) {
        viewHolder.activeSwitch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                isFade = true;
                return false;
            }
        });
        viewHolder.activeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if(isFade) {
                    if (checked) {
                        alarm.setActive(true);
                        viewHolder.timeTextView.setTextColor(resources.getColor(R.color.colorAccent));
                        viewHolder.repetitionTextView.setTextColor(resources.getColor(R.color.colorDarkText));
                        // schedule alarm
                        Log.i(TAG, "scheduling alarm: " + alarm.id);
                        mAlarmHandler.scheduleAlarm(alarm);
                    } else {
                        alarm.setActive(false);
                        viewHolder.timeTextView.setTextColor(resources.getColor(R.color.colorGrey500));
                        viewHolder.repetitionTextView.setTextColor(resources.getColor(R.color.colorGrey500));
                        mAlarmHandler.cancelAlarm(alarm);
                    }
                }
                // Update database and schedule alarm
                Log.i(TAG, "updating database with alarm: " + alarm.id);
                mAlarmViewModel.updateActive(alarm);
            }

        });
    }

    /**
     * Add OnClickListener to alarm's edit button to open EditAlarmActivity (AddAlarmActivity.java)
     *
     * @param alarm      Alarm object
     * @param viewHolder Alarm's ViewHolder object, containing edit button
     */
    private void addEditButtonListener(final Alarm alarm, final AlarmViewHolder viewHolder) {
        viewHolder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context, AddAlarmActivity.class);

                Bundle args = new Bundle();
                args.putParcelable(AddAlarmActivity.EXTRA_ALARM, alarm);
                intent.putExtra(AddAlarmActivity.EXTRA_BUNDLE, args);

                ((MainActivity) context).startActivityForResult(intent, MainActivity.EDIT_ALARM_ACTIVITY_REQUEST_CODE);
            }
        });
    }

}
