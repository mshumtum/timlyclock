package com.amtech.timelyclock.Ui.genralScreen

import android.os.Bundle
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.amtech.timelyclock.R
import com.amtech.timelyclock.util.Constants
import kotlinx.android.synthetic.main.activity_add_alarm.*
import kotlinx.android.synthetic.main.contact_us.*
import kotlinx.android.synthetic.main.contact_us.mainBackground

class ContactUsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.contact_us)
        mainBackground.setBackgroundResource(Constants.getTheme())
        submit.setOnClickListener { view -> submitData()}
    }

    private fun submitData() {

        if(validation()){
            showToast("Submitted ,Shortly revert you")
            finish()
        }
    }

    private fun validation(): Boolean {
        if(nameTxt.length() >0){
            if(emailTxt.length() > 5){
                if(phoneTxt.length() >9){
                    if(msgTxt.length() > 1){
                        return true
                    }else{
                        showToast("Enter Proper message")
                        return false
                    }
                }else{
                    return false
                    showToast("Enter valid contact number")
                }
            }else{
                return false
                showToast("Enter valid email")
            }
        }else{
            showToast("Plaese enter name")
            return false
        }
    }

    private fun showToast(s: String) {
        Toast.makeText(this,s,Toast.LENGTH_SHORT).show()
    }
}