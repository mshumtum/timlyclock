package com.amtech.timelyclock.Ui;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amtech.timelyclock.R;
import com.amtech.timelyclock.Ui.adapter.WeekendAdapter;
import com.amtech.timelyclock.alarmmanager.AlarmHandler;
import com.amtech.timelyclock.db.Alarm;
import com.amtech.timelyclock.util.Constants;
import com.amtech.timelyclock.viewmodel.AlarmViewModel;
import com.dpro.widgets.WeekdaysPicker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ca.antonious.materialdaypicker.MaterialDayPicker;
import ca.antonious.materialdaypicker.SingleSelectionMode;

import static com.amtech.timelyclock.util.Constants.darkBlue;
import static com.amtech.timelyclock.util.Constants.lightBlue;

/**
 * Used to create and edit alarms, depending on REQUEST_CODE
 */
public class AddAlarmActivity extends AppCompatActivity {

    // Key values for returning intent.
    public static final String EXTRA_BUNDLE = "com.manveerbasra.ontime.AddAlarmActivity.BUNDLE";
    public static final String EXTRA_ALARM = "com.manveerbasra.ontime.AddAlarmActivity.ALARM";
    public static final String EXTRA_DELETE = "com.manveerbasra.ontime.AddAlarmActivity.DELETE";
    private static final int SET_START_LOCATION_ACTIVITY_REQUEST_CODE = 1;
    private static final int SET_END_LOCATION_ACTIVITY_REQUEST_CODE = 2;
    private final String TAG = "AddAlarmActivity";
    RecyclerView weekdays;
    String[] days;
    ImageView delete;
    daysClick daysClick;
    SwitchCompat setAlarm;
    private AlarmViewModel alarmViewModel;
    private int mCurrRequestCode; // current request code - static values in MainActivity
    private Alarm mAlarm;
    // Data objects
    private Calendar calendar;
    // View objects
    private TextView mTimeTextView, activityName;
    private TextView mRepeatTextView;
    private boolean[] activeDays;
    TimePicker timePicker;
    AlarmHandler mAlarmHandler;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_alarm);
        RelativeLayout mainBackground = findViewById(R.id.mainBackground);
        mainBackground.setBackgroundResource(Constants.getTheme());
        mAlarmHandler = new AlarmHandler(this, mainBackground);


        timePicker=findViewById(R.id.time_Picker);

        daysClick = new daysClick() {
            @Override
            public void onClick(boolean[] daysArray) {
                mAlarm.activeDays = daysArray;
                String formattedActiveDays = Alarm.getStringOfActiveDays(mAlarm.activeDays);
                mRepeatTextView.setText(formattedActiveDays);
            }
        };

        days = getResources().getStringArray(R.array.days_of_week);
        calendar = Calendar.getInstance();
        mTimeTextView = findViewById(R.id.add_alarm_time_text);
        setAlarm = findViewById(R.id.setAlarm);
        weekdays = findViewById(R.id.weekdays);
        delete = findViewById(R.id.delete);
        activityName = findViewById(R.id.activityName);
        mRepeatTextView = findViewById(R.id.add_alarm_repeat_text);

        // Get a new or existing ViewModel from the ViewModelProvider.
        alarmViewModel = ViewModelProviders.of(this).get(AlarmViewModel.class);

        Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_BUNDLE)) { // Activity called to edit an alarm.
            Bundle args = intent.getBundleExtra(EXTRA_BUNDLE);
            mAlarm = args.getParcelable(EXTRA_ALARM);
            mCurrRequestCode = MainActivity.EDIT_ALARM_ACTIVITY_REQUEST_CODE;
        } else {
            mCurrRequestCode = MainActivity.NEW_ALARM_ACTIVITY_REQUEST_CODE;
        }

        if (mAlarm != null) {
            mTimeTextView.setText(mAlarm.getStringTime());
            timePicker.setHour(mAlarm.time.getHours());
            timePicker.setMinute(mAlarm.time.getMinutes());
            mRepeatTextView.setText(mAlarm.getStringOfActiveDays());
            activityName.setText(R.string.edit_alarm);
            activeDays = mAlarm.activeDays;
            delete.setVisibility(View.VISIBLE);
            setAlarm.setChecked(mAlarm.active);
            Log.e(TAG, "onCreate: " + mAlarm.active);

        } else {
            mAlarm = new Alarm();
            setAlarm.setChecked(true);
            setAlarm.setVisibility(View.GONE);
            activeDays = new boolean[7];
            mAlarm.activeDays = new boolean[7];
            setInitialAlarmTime();
            mRepeatTextView.setText(R.string.never);
            mAlarm.active = false;
        }

       // addSetTimeLayoutListener();
        addAlarmListener();
        setWeekend();
        addToggelListener();
        addBackCancelListener();
        addDeleteButtonListener();
    }

    private void addToggelListener() {
        setAlarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e(TAG, "onCheckedChanged: " + isChecked);
                mAlarm.active = isChecked;
            }
        });
    }

    private void setWeekend() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 7);
        weekdays.setLayoutManager(gridLayoutManager);
        WeekendAdapter adapter = new WeekendAdapter(this, days, activeDays, daysClick);
        weekdays.setAdapter(adapter);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void addAlarmListener() {



        Button addAlarm = findViewById(R.id.addAlarm);
        addAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAlarm.setTime(getFormattedTime(timePicker.getCurrentHour(),timePicker.getCurrentMinute()));
                Intent replyIntent = new Intent();
                if (mCurrRequestCode == MainActivity.EDIT_ALARM_ACTIVITY_REQUEST_CODE) {
                    alarmViewModel.update(mAlarm);
                  mAlarmHandler.scheduleAlarm(mAlarm);
                } else {
                    alarmViewModel.insert(mAlarm);

                }
                setResult(RESULT_OK, replyIntent);
                finish();

            }
        });

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setInitialAlarmTime() {
        // Get time and set it to alarm time TextView
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        String currentTime = getFormattedTime(hour, minute);
        mTimeTextView.setText(currentTime);
        mAlarm.setTime(currentTime);

    }

    private void addDeleteButtonListener() {
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent replyIntent = new Intent();

                alarmViewModel.delete(mAlarm);
                replyIntent.putExtra(EXTRA_DELETE, true);

                setResult(RESULT_OK, replyIntent);
                finish();
            }
        });
    }

    private void addSetTimeLayoutListener() {
        RelativeLayout setTimeButton = findViewById(R.id.add_alarm_time_layout);

        setTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int hour, minute;
                if (mAlarm.time == null) {
                    hour = calendar.get(Calendar.HOUR_OF_DAY);
                    minute = calendar.get(Calendar.MINUTE);
                } else {
                    Calendar calendar = GregorianCalendar.getInstance();
                    calendar.setTime(mAlarm.time);
                    hour = calendar.get(Calendar.HOUR_OF_DAY);
                    minute = calendar.get(Calendar.MINUTE);
                }

                TimePickerDialog timePicker;
                timePicker = new TimePickerDialog(AddAlarmActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String formattedTime = getFormattedTime(selectedHour, selectedMinute);
                        mAlarm.setTime(formattedTime);
                        mTimeTextView.setText(formattedTime);
                    }
                }, hour, minute, false);
                timePicker.setTitle("Select Time");
                timePicker.show();
            }
        });
    }

    private String getFormattedTime(int hour, int minute) {
        String meridian = "AM";
        if (hour >= 12) {
            meridian = "PM";
        }

        if (hour > 12) {
            hour -= 12;
        } else if (hour == 0) {
            hour = 12;
        }

        String formattedTime;
        if (minute < 10) {
            formattedTime = hour + ":0" + minute + " " + meridian;
        } else {
            formattedTime = hour + ":" + minute + " " + meridian;
        }

        return formattedTime;
    }

    void addBackCancelListener() {
        Button cancel = findViewById(R.id.cancel);
        ImageView back = findViewById(R.id.back);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}

