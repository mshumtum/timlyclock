package com.amtech.timelyclock.Ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaMetadataRetriever;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amtech.timelyclock.R;
import com.amtech.timelyclock.Ui.adapter.AlarmListAdapter;
import com.amtech.timelyclock.db.Alarm;
import com.amtech.timelyclock.util.Constants;
import com.amtech.timelyclock.viewmodel.AlarmViewModel;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final String TAG = "MainActivity";

    public static final int NEW_ALARM_ACTIVITY_REQUEST_CODE = 1;
    public static final int EDIT_ALARM_ACTIVITY_REQUEST_CODE = 2;
    //access AlarmDatabase
    private AlarmViewModel alarmViewModel;
    private View snackbarAnchor;
    LinearLayout tabHereLL;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpView();
    }

    private void setUpView() {
        RecyclerView recyclerView = findViewById(R.id.alarmView);
        ImageView addAlarm = findViewById(R.id.addAlarm);
        ImageView setting = findViewById(R.id.setting);
        tabHereLL = findViewById(R.id.tabHereLL);
        snackbarAnchor = findViewById(R.id.mainBackground);
        //snackbarAnchor.setBackground(Constants.gradiant(darkBlue,lightBlue));
        snackbarAnchor.setBackgroundResource(Constants.getTheme());

        final AlarmListAdapter adapter = new AlarmListAdapter(this);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);


        // Get a new or existing ViewModel from the ViewModelProvider.
        alarmViewModel = ViewModelProviders.of(this).get(AlarmViewModel.class);
        tabHereLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddAlarmActivity.class);
                startActivityForResult(intent, NEW_ALARM_ACTIVITY_REQUEST_CODE);
            }
        });
        // Add an observer on the LiveData returned by getAllAlarms.
        alarmViewModel.getAllAlarms().observe(this, new Observer<List<Alarm>>() {
            @Override
            public void onChanged(@Nullable final List<Alarm> alarms) {
                // Update the cached copy of the words in the adapter.
                if(alarms.size() >0){
                adapter.setAlarms(alarms);
                }else {
                    tabHereLL.setVisibility(View.VISIBLE);
                }
            }
        });
        addAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddAlarmActivity.class);
                startActivityForResult(intent, NEW_ALARM_ACTIVITY_REQUEST_CODE);
            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startActivity(new Intent(MainActivity.this,SettingActivity.class));
            }
        });

    }

  /*  private void getAlarmRingTone() {
       SharedPreferences ringPref= getSharedPreferences(RINGTONE_PREF, 0);
        SharedPreferences fadeInPref= getSharedPreferences(FADE_IN_PREF, 0);
        String ringStr=ringPref.getString(RINGTONE_NAME, "");
        FADE=fadeInPref.getBoolean(FADE_BOOL,false);
        if(ringStr.length() == 0){
            ringTone=getAlarmUri();
        }else{
            ringTone= Uri.parse(ringStr);
        }
        Log.e(TAG, "getAlarmRingTone: "+ringTone );
    }
    private static Uri getAlarmUri() {
        Uri alert = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_ALARM);
        if (alert == null) {
            alert = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if (alert == null) {
                alert = RingtoneManager
                        .getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }
        return alert;
    }*/

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (data.hasExtra(AddAlarmActivity.EXTRA_DELETE)) {
                Snackbar.make(snackbarAnchor, R.string.alarm_deleted, Snackbar.LENGTH_SHORT).show();
            } else {
                Snackbar.make(snackbarAnchor, R.string.alarm_saved, Snackbar.LENGTH_SHORT).show();
            }

        } else {
            Snackbar.make(snackbarAnchor, R.string.alarm_not_saved, Snackbar.LENGTH_SHORT).show();
        }
    }
}
