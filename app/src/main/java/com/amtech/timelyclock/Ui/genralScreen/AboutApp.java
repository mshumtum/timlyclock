package com.amtech.timelyclock.Ui.genralScreen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.amtech.timelyclock.R;
import com.amtech.timelyclock.util.Constants;

import org.w3c.dom.Text;

public class AboutApp extends AppCompatActivity {
    String packageName="com.amtech.timelyclock";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_app);

        LinearLayout mainBackground=findViewById(R.id.mainBackground);
        mainBackground.setBackgroundResource(Constants.getTheme());

        ImageView setting=findViewById(R.id.setting);
        ImageView back=findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        setting.setVisibility(View.GONE);
        TextView appInfo=findViewById(R.id.appInfo);
        appInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSettingScreen();
            }
        });
        TextView openLicences=findViewById(R.id.openLicences);
        openLicences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(AboutApp.this, "Give us a link", Toast.LENGTH_SHORT).show();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    void openSettingScreen(){
        try {
            Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setData(Uri.parse("package:" + packageName));
            startActivity(intent);

        } catch ( ActivityNotFoundException e ) {
            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
            startActivity(intent);

        }
    }
}
