package com.amtech.timelyclock.Ui.adapter;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.amtech.timelyclock.R;
import com.amtech.timelyclock.Ui.AddAlarmActivity;
import com.amtech.timelyclock.Ui.daysClick;

import static android.content.ContentValues.TAG;

public class WeekendAdapter extends RecyclerView.Adapter<WeekendAdapter.ViewHolder> {
    AddAlarmActivity addAlarmActivity;
    String[] days;
    boolean[] activeDays;
    daysClick daysClick;

    public WeekendAdapter(AddAlarmActivity addAlarmActivity, String[] days, boolean[] activeDays, daysClick daysClick) {
        this.addAlarmActivity = addAlarmActivity;
        this.days = days;
        this.activeDays = activeDays;
        this.daysClick = daysClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(addAlarmActivity).inflate(R.layout.days_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.daysName.setText(days[position]);

        holder.selectDays.setBackgroundTintList(activeDays[position] ? ContextCompat.getColorStateList(addAlarmActivity, R.color.colorPrimary): ContextCompat.getColorStateList(addAlarmActivity, R.color.darkBlue));
        holder.selectDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(activeDays[position]){
                    activeDays[position]=false;
                }else{

                    activeDays[position]=true;
                }
                holder.selectDays.setBackgroundTintList(activeDays[position] ? ContextCompat.getColorStateList(addAlarmActivity, R.color.colorPrimary): ContextCompat.getColorStateList(addAlarmActivity, R.color.darkBlue));
                daysClick.onClick(activeDays);
            }
        });


    }

    @Override
    public int getItemCount() {
        return days.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout selectDays;
        TextView daysName;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            selectDays=itemView.findViewById(R.id.selectDays);
            daysName=itemView.findViewById(R.id.daysName);
        }
    }
}
