package com.amtech.timelyclock.Ui.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.amtech.timelyclock.R;
import com.amtech.timelyclock.Ui.RingtoneClick;
import com.amtech.timelyclock.viewmodel.RingtoneUriModel;

import java.util.ArrayList;

public class DialogRingtoneFragment extends RecyclerView.Adapter<DialogRingtoneFragment.MyView> {
    FragmentActivity activity;
    ArrayList<RingtoneUriModel> nameArray;
    RingtoneClick ringtoneClick;
    public DialogRingtoneFragment(FragmentActivity activity, ArrayList<RingtoneUriModel> nameArray, RingtoneClick ringtoneClick) {
        this.activity=activity;
        this.nameArray=nameArray;
        this.ringtoneClick=ringtoneClick;
    }

    @NonNull
    @Override
    public MyView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.model,parent,false);
        MyView holder=new MyView(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyView holder, final int position) {
        holder.nameTxt.setText(nameArray.get(position).getName());
        holder.playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.playBtn.setVisibility(View.GONE);
                holder.pauseBtn.setVisibility(View.VISIBLE);
                ringtoneClick.onClick(position,1);
            }
        });
        holder.pauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.playBtn.setVisibility(View.VISIBLE);
                holder.pauseBtn.setVisibility(View.GONE);

                ringtoneClick.onClick(position,2);
            }
        });

        holder.nameTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ringtoneClick.onClick(position,3);
            }
        });

    }

    @Override
    public int getItemCount() {
        return nameArray.size();
    }

    public class MyView extends RecyclerView.ViewHolder {
        TextView nameTxt;
        ImageView pauseBtn,playBtn;
        public MyView(@NonNull View itemView) {
            super(itemView);
            nameTxt=itemView.findViewById(R.id.nameTxt);
            pauseBtn=itemView.findViewById(R.id.pauseBtn);
            playBtn=itemView.findViewById(R.id.playBtn);
        }
    }

}
