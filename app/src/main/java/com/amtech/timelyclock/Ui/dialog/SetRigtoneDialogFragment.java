package com.amtech.timelyclock.Ui.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amtech.timelyclock.R;
import com.amtech.timelyclock.Ui.RingtoneClick;
import com.amtech.timelyclock.Ui.adapter.DialogRingtoneFragment;
import com.amtech.timelyclock.viewmodel.RingtoneUriModel;

import java.io.IOException;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;
import static com.amtech.timelyclock.util.Constants.ringtonePrevious;

public class SetRigtoneDialogFragment extends DialogFragment {

    private ArrayList<RingtoneUriModel> nameArray=new ArrayList<>();
    RecyclerView rv;
    DialogRingtoneFragment adapter;
    RingtoneClick ringtoneClick;
    MediaPlayer mediaPlayer;
    int previous=0;
    public interface OnDialogCompleteListener {
        void onDialogComplete(String name, Uri uri);
    }
    private OnDialogCompleteListener completeListener;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.completeListener = (OnDialogCompleteListener) activity;
        } catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnCompleteListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mediaPlayer = new MediaPlayer();

        RingtoneManager manager = new RingtoneManager(getActivity());
        manager.setType(RingtoneManager.TYPE_RINGTONE);
        Cursor cursor = manager.getCursor();
        while (cursor.moveToNext()) {
            String title = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);
            Uri ringtoneURI = manager.getRingtoneUri(cursor.getPosition());
            Log.e(TAG, "onCreate: "+title );
            RingtoneUriModel uriModel=new RingtoneUriModel(title,ringtoneURI);
            nameArray.add(uriModel);
            // Do something with the title and the URI of ringtone
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fraglayout,container);
        ringtoneClick=new RingtoneClick() {
            @Override
            public void onClick(int position,int click) {
                if(click==1){
                    playRingtone(nameArray.get(position).getUri());
                }else if(click == 2){
                    mediaPlayer.reset();
                }
                else{

                    mediaPlayer.reset();
                    completeListener.onDialogComplete(nameArray.get(position).getName(),nameArray.get(position).getUri());
                }
            }
        };

        //RECYCER
        rv= (RecyclerView) rootView.findViewById(R.id.mRecyerID);
        rv.setLayoutManager(new LinearLayoutManager(this.getActivity()));

        //ADAPTER
        adapter=new DialogRingtoneFragment(this.getActivity(),nameArray,ringtoneClick);
        rv.setAdapter(adapter);

        this.getDialog().setTitle("Select Rigntone");


        return rootView;
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    private void playRingtone(Uri uri) {

        mediaPlayer.reset();
        try {
            // mediaPlayer.setDataSource(String.valueOf(myUri));
            mediaPlayer.setDataSource(getActivity(),uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.start();

    }
}
