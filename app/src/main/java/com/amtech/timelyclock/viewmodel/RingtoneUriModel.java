package com.amtech.timelyclock.viewmodel;

import android.net.Uri;

public class RingtoneUriModel {
    String name;
    Uri uri;

    public RingtoneUriModel(String name, Uri uri) {
        this.name = name;
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public Uri getUri() {
        return uri;
    }
}
