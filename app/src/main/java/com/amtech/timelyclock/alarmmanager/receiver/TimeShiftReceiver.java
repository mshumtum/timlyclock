package com.amtech.timelyclock.alarmmanager.receiver;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.amtech.timelyclock.alarmmanager.AlarmHandler;
import com.amtech.timelyclock.db.Alarm;
import com.amtech.timelyclock.util.AlarmRepository;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * BroadcastReceiver to process time shift required to alarm time
 * and set respective alarm.
 */
public class TimeShiftReceiver extends BroadcastReceiver {

    private final String TAG = "TimeShiftReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "received alarm time shift intent");

        int alarmID = intent.getIntExtra(AlarmHandler.EXTRA_ID, 0);

        // Get alarm from repository
        AlarmRepository repository = new AlarmRepository((Application) context.getApplicationContext());
        Alarm alarm;
        try {
            alarm = repository.getAlarmById(alarmID);
        } catch (InterruptedException | ExecutionException e) {
            Log.e(TAG, "Error when retrieving alarm by id: " + alarmID);
            e.printStackTrace();
            alarm = null;
        }

        AlarmHandler alarmHandler = new AlarmHandler(context, null);
        alarmHandler.scheduleAlarmWithTime((int) TimeUnit.HOURS.toMillis(1), alarmID);

    }
}
