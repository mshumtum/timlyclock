package com.amtech.timelyclock.alarmmanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import java.io.IOException;
/**
 * Singleton class to control Alarm Ringing Sound
 */
public class AlarmSoundControl {

    private static AlarmSoundControl mINSTANCE;
    private final String TAG = "AlarmSoundControl";
    SharedPreferences ringPref, fadeInPref;
    private MediaPlayer mMediaPlayer;


    private AlarmSoundControl() {
    }

    public static AlarmSoundControl getInstance() {
        if (mINSTANCE == null) {
            mINSTANCE = new AlarmSoundControl();
        }
        return mINSTANCE;
    }

    public static float getDeviceVolume(AudioManager audioManager) {
        int volumeLevel = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        return (float) volumeLevel / maxVolume;
    }

    /**
     * Play Alarm Sound
     */
    public void playAlarmSound(Context context, Uri ringTone, boolean FADE) {


        mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(context, ringTone);
            final AudioManager audioManager = (AudioManager) context
                    .getSystemService(Context.AUDIO_SERVICE);
            if (audioManager != null &&
                    audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.prepare();

                //if fade in
                if (FADE) {
                    fadeIn(mMediaPlayer.getDuration(), audioManager);

                } else {
                    //else
                    mMediaPlayer.start();

                }
            }

        } catch (IOException e) {
            System.out.println("Can't read Alarm uri: ");
        }
    }

    public void fadeIn(final int duration, AudioManager audioManager) {
        final float deviceVolume = getDeviceVolume(audioManager);
        final Handler h = new Handler();
        h.postDelayed(new Runnable() {
            private float time = 0.0f;
            private float volume = 0.0f;

            @Override
            public void run() {
                if (!mMediaPlayer.isPlaying())
                    mMediaPlayer.start();
                // can call h again after work!
                time += 100;
                volume = (deviceVolume * time) / duration;
                mMediaPlayer.setVolume(volume, volume);
                if (time < duration)
                    h.postDelayed(this, 100);
            }
        }, 100); // 1 second delay (takes millis)

    }

    /**
     * Stop Alarm Sound currently playing
     */
    public void stopAlarmSound() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
        }
    }

    /**
     * Get alarm sound, try to get default, then notification, then ringtone
     *
     * @return URI for alarm sound
     */

}