package com.amtech.timelyclock.alarmmanager.receiver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.transition.Fade;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.amtech.timelyclock.R;
import com.amtech.timelyclock.alarmmanager.AlarmHandler;
import com.amtech.timelyclock.alarmmanager.AlarmSoundControl;

import static com.amtech.timelyclock.util.Constants.FADE_BOOL;
import static com.amtech.timelyclock.util.Constants.FADE_IN_PREF;
import static com.amtech.timelyclock.util.Constants.RINGTONE_NAME;
import static com.amtech.timelyclock.util.Constants.RINGTONE_PREF;
import static com.amtech.timelyclock.util.Constants.RINGTONE_URI;
import static com.amtech.timelyclock.util.Constants.VIBRATION_BOOL;
import static com.amtech.timelyclock.util.Constants.VIBRATION_PREF;
import static com.amtech.timelyclock.util.Constants.getAlarmUri;

/**
 * By Munish on 11-08-2020
 * BroadcastReceiver to setup and display alarm notification
 */


public class AlarmReceiver extends BroadcastReceiver {

    private final String TAG = "AlarmReceiver";
    private final String CHANNEL_ID = "AlarmReceiverChannel";
    public Uri ringTone;
    public boolean FADE;
    private SharedPreferences mPreferences;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "received alarm intent");
        SharedPreferences ringPref = context.getSharedPreferences(RINGTONE_PREF, 0);
        SharedPreferences fadeInPref = context.getSharedPreferences(FADE_IN_PREF, 0);
        String ringStr = ringPref.getString(RINGTONE_URI, "");
        boolean FADE = fadeInPref.getBoolean(FADE_BOOL, false);
        Log.e(TAG, "onReceive: " + ringStr);
        Log.e(TAG, "onReceive: " + FADE);
        if (ringStr.length() == 0) {
            ringTone = getAlarmUri();
        } else {
            ringTone = Uri.parse(ringStr);
        }

        Log.e(TAG, "onReceive: " + ringTone);
        Log.e(TAG, "onReceive: " + FADE);

        //  Toast.makeText(context, "hello", Toast.LENGTH_SHORT).show();

        // Initialize preferences
        // mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mPreferences = context.getSharedPreferences(VIBRATION_PREF, 0);

        int alarmID = intent.getIntExtra(AlarmHandler.EXTRA_ID, 0);
        Log.e(TAG, "onReceive: " + alarmID);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Create and add notification channel
        NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, TAG, NotificationManager.IMPORTANCE_HIGH);
        if (notificationManager != null) notificationManager.createNotificationChannel(mChannel);

        Notification notification = buildNotification(
                context,
                getStopAlarmIntent(context, alarmID),
                getSnoozeAlarmIntent(context, alarmID)
        );

        // Play alarm ringing sound
        AlarmSoundControl alarmSoundControl = AlarmSoundControl.getInstance();
        alarmSoundControl.playAlarmSound(context.getApplicationContext(), ringTone, FADE);

        Log.i(TAG, "displaying notification for alarm " + alarmID);
        if (notificationManager != null) notificationManager.notify(alarmID, notification);
    }

    /**
     * Get PendingIntent to Snooze Alarm
     *
     * @param context current App context
     * @param alarmID ID of alarm to handle
     * @return PendingIntent to AlarmSnooze(Broadcast)Receiver
     */
    private PendingIntent getSnoozeAlarmIntent(Context context, int alarmID) {
        Intent snoozeAlarmIntent = new Intent(context, AlarmSnoozeReceiver.class);
        snoozeAlarmIntent.putExtra(AlarmHandler.EXTRA_ID, alarmID);
        snoozeAlarmIntent.setAction("Snooze Alarm");
        return PendingIntent.getBroadcast(context, 0, snoozeAlarmIntent, 0);
    }

    /**
     * Get PendingIntent to Stop Alarm
     *
     * @param context current App context
     * @param alarmID ID of alarm to handle
     * @return PendingIntent to AlarmStop(Broadcast)Receiver
     */
    private PendingIntent getStopAlarmIntent(Context context, int alarmID) {
        Intent stopAlarmIntent = new Intent(context, AlarmStopReceiver.class);
        stopAlarmIntent.putExtra(AlarmHandler.EXTRA_ID, alarmID);
        stopAlarmIntent.setAction("Stop Alarm");
        return PendingIntent.getBroadcast(context, 0, stopAlarmIntent, 0);
    }

    /**
     * Build the alarm notification
     *
     * @param context                  current App context
     * @param stopAlarmPendingIntent   PendingIntent object to stop the alarm ringing
     * @param snoozeAlarmPendingIntent Pending Intent object to snooze the alarm ringing
     * @return a Notification object of the built alarmic_launcher_foreground notification
     */
    private Notification buildNotification(Context context, PendingIntent stopAlarmPendingIntent, PendingIntent snoozeAlarmPendingIntent) {
        NotificationCompat.Builder notification = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(context.getString(R.string.alarm_notification_title))
                .addAction(R.drawable.ic_launcher_background, context.getString(R.string.stop),
                        stopAlarmPendingIntent)
                .addAction(R.drawable.ic_launcher_background, context.getString(R.string.snooze),
                        snoozeAlarmPendingIntent);
        if (mPreferences.getBoolean(VIBRATION_BOOL, true)) {
            Log.i(TAG, "notification vibrate set to false");
            notification.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});

        } else {
            notification.setVibrate(null);

        }

        return notification.build();
    }
}
