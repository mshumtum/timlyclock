package com.amtech.timelyclock.db;

import android.content.Context;
import android.os.AsyncTask;


import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.amtech.timelyclock.db.converter.BooleanArrayConverter;
import com.amtech.timelyclock.db.converter.DateConverter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Backend Database
 */
@Database(entities = {Alarm.class}, version = 1, exportSchema = false)
@TypeConverters({DateConverter.class, BooleanArrayConverter.class})
public abstract class AlarmDatabase extends RoomDatabase {

    private static AlarmDatabase mINSTANCE;

    @VisibleForTesting
    public static final String DATABASE_NAME = "alarm-db";

    public abstract AlarmDao alarmModel();

    public static AlarmDatabase getInstance(final Context context) {
        if (mINSTANCE == null) {
            synchronized (AlarmDatabase.class) {
                if (mINSTANCE == null) {
                    mINSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AlarmDatabase.class, DATABASE_NAME)
                            .fallbackToDestructiveMigration() // TODO Add Proper Migration
                            .addCallback(roomDatabaseCallback)
                            .build();
                }
            }
        }
        return mINSTANCE;
    }

    /**
     * Override the onCreate method to populate the database.
     */
    private static RoomDatabase.Callback roomDatabaseCallback = new RoomDatabase.Callback() {

        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsync(mINSTANCE).execute();
        }

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
        }
    };

    /**
     * Populate the database in the background when app is first created.
     */
    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final AlarmDao alarmModel;

        PopulateDbAsync(AlarmDatabase db) {
            alarmModel = db.alarmModel();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            String str = "08:00 am";
            DateFormat formatter = new SimpleDateFormat("hh:mm aa");
            Date time = null;
            try {
                time = formatter.parse(str);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            boolean[] activeDays = {false, true, true, true, true, true, false};


            Alarm alarm = new Alarm(
                    time, false, activeDays
          /*          start, end,
                    startPlace, endPlace,
                    "drive"*/
            );
            alarmModel.insert(alarm);
            return null;
        }
    }
}
