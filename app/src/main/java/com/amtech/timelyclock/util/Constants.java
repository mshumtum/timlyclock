package com.amtech.timelyclock.util;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.media.RingtoneManager;
import android.net.Uri;

import com.amtech.timelyclock.R;
import com.amtech.timelyclock.Ui.MainActivity;

import java.util.Calendar;

public class Constants {

    public static final String darkBlue = "#0d1141";
    public static final String lightBlue = "#2b1545";
    public static final String darkOrage = "#f87b57";
    public static final String lightOrage = "#fbcd5d";
    public static final String darkSkyBlue = "#216d95";
    public static final String lightSkyBlue = "#8bc0ce";
    public static final String darkBrightBlue = "#1f3258";
    public static final String lightBrightBlue = "#5986c9";
    public static int ringtonePrevious = 0;
    public static String RINGTONE_PREF = "ringtone_pref";
    public static String RINGTONE_NAME = "ring_name";
    public static String RINGTONE_URI = "ring_uri";
    public static String FADE_IN_PREF = "fade_in_pref";
    public static String FADE_BOOL = "fade_bool";
    public static String VIBRATION_PREF = "vibration_pref";
    public static String VIBRATION_BOOL = "vibration_bool";

    public static boolean FADE;
    public static Uri getAlarmUri() {
        Uri alert = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_ALARM);
        if (alert == null) {
            alert = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if (alert == null) {
                alert = RingtoneManager
                        .getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }
        return alert;
    }

    public static GradientDrawable gradiant(String a, String b) {
        GradientDrawable gd = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM,
                new int[]{Color.parseColor(a),
                        Color.parseColor(b)});
        gd.setCornerRadius(0f);
        return gd;
    }

    public static int getTheme() {
        Calendar rightNow = Calendar.getInstance();
        int currentHourIn24Format = rightNow.get(Calendar.HOUR_OF_DAY);
        if(currentHourIn24Format < 5 && currentHourIn24Format >= 20 ){
            //more dark
            return R.mipmap.dark;

        }else if(currentHourIn24Format >= 5 && currentHourIn24Format < 10){
            //morning
            return R.mipmap.light_bright;
        }else if(currentHourIn24Format >= 10 && currentHourIn24Format <= 16){
            //full morning
            return R.mipmap.more_bright;
        }else{
            //evening
            return R.mipmap.light_dark;
        }

    }

}
